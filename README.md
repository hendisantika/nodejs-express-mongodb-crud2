# NodeJS Express MongoDB CRUD

Run this project by this command :
1. `git clone https://gitlab.com/hendisantika/nodejs-express-mongodb-crud2.git`
2. `npm install`
3. `node index`

### Screen shot

Home Page

![Home Page](img/home.png "Home Page")

List Tasks

![List Tasks](img/list.png "List Task")

Add New Task

![Add New Task](img/add.png "Add  New Task")

Edit Task

![Edit Task](img/edit.png "Edit Task")
